const { json } = require('body-parser');
const express = require('express'); // Mặc định mới vô cài cái này. express là 1 module của node.js

const app = express();

app.use(express.json());

app.get('/viethiep/:param1/:param2', (request, response) => { // Tạo API phương thức GET, lấy giá trị từ request params và thử trên Postman
    const param1 = request.params.param1;
    const param2 = request.params.param2;
    response.json({
        "id": 1,
        "drinkName": "cocacola",
        "param1" : param1,
        "param2" : param2
    });
});

app.get('/viethiep', (req, res) => { 
    res.json({
        "query": req.query // Tạo API phương thức GET, lấy giá trị từ request query và thử trên Postman
    });
});

app.post('/viethiep', (req, res) => {
    const bodyJson = req.body;
    res.json({
        body: bodyJson // Tạo API phương thức POST, lấy giá trị từ request body raw json và thử trên Postman
    });
});

app.put('/viethiep', (req, res) => {
    res.json(`object người dùng vừa put là ${JSON.stringify(req.body)}`); //Tạo API phương thức POST, lấy giá trị từ request body raw json và thử trên Postman
});

app.delete('/viethiep', (req, res) => {
    res.json(`delete thành công`)
});

app.listen(8000, () => {
    console.log(`App is listening on port: ${8000}`);
});